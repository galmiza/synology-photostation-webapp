# Welcome

This webapp lets you to browse through the photo collection on your Synology NAS server on the go.

It is not intented to replace the DS Photo application as it doesn't allow photo edition nor render videos, yet it offers interesting viewing and sharing features.

**Tile view or stream view**

DS Photo offers tile view and slideshow.  
I needed a stream view like Instagram to have higher resolution preview of the pictures without opening them.

_Below an exemple of tile and stream view from the webapp on a mobile device._

![](../../downloads/Screenshot_2019-01-13-11-41-49-779_com.ecosia.android_400x.png)
![](../../downloads/Screenshot_2019-01-13-11-42-16-968_com.ecosia.android_400x.png)

**Public links with for multiple albums sharing**

DS Photo generates public links to share a single album.  
I needed a permanent and unique public link to share a whole subset of my photo collection.

_The webapp works with tokens that define permissions on a list of folders. Permissions of parent folders are propagated to subfolders._

# Installation

Clone the repository at the root of your web server of your Synology diskstation.

```
cd /volume1/web
git clone https://galmiza@bitbucket.org/galmiza/synology-photostation-webapp.git
```

Rename the repository folder to a more convenient name.

```
mv synology-photostation-webapp album
```

You can now access the application !

```
https://[your Synology DNS]/album
```

# Configuration

The configuration of the webapp is stored in the file **config.php**. Changes are applied immediately.

#### Supported formats: extensions

Configuration element **extensions** represents the filter on file extensions that the webapp must render.

#### Public links permissions: tokens

Configuration element **tokens** represents the list of tokens and their permission on the photo collection.

Each token defines a list of folders to which it grants access.

_Example,_

```
"AZERTY" => [
  "2018",
  "2012/Summer with Maria",
  "2013/Guillaume/Judo"
]
```

_Defines the token "AZERTY" that grants access to 3 folders and its subfolders._

#### Relative path to the photo directory: root

Configuration element **root** represents the relative path of the photo directory from the webapp directory.

```
/volume1/photo      # DS Photo directory
/volume1/web/album  # webapp directory
```

Hence, the relative path.

```
../../photo
```


# Design

The design of the application is minimalistic:

* a web application that renders the albums and pictures in a tile or stream view
* a PHP backend that runs on the Synology device to access the photo collection

#### Web application

The web application is a single page application built with the popular framework AngularJS.

It uses the API of the PHP backend.

#### PHP backend

The PHP backend exposes an API that handles a single endpoint with three parameters: **path**, **target** and **token**.

* **path** represents the path of the folder to read
* **target** represents the path of the file to read
* **token** represents the token that defines read permissions

_Examples_

```
# read content of album "2018"
curl -k https://[your Synology DNS]/album/index.php?path=2018\&token=AZERTY
[
  {
    "path":"2018/MyPicture1.jpg",
    "type":"file",
    "thumbnail":"2018\/@eaDir\/MyPicture1.jpg\/SYNOPHOTO_THUMB_B.jpg"
  },{
    "path":"2018/MyFolderA",
    "type":"folder",
    "thumbnail":"2018\/MyFolderA\/@eaDir\/MyPicture2.jpg\/SYNOPHOTO_THUMB_B.jpg"
  }
]
```

```
# read a picture
curl -k https://[your Synology DNS]/album/index.php?target=2018/MyPicture1.jpg\&token=AZERTY
```


# Integration with DS Photo

DS Photo automatically adds the folder **@eaDir** in every directory of the file structure. This folder keeps meta information used by the DS Photo application.

Names of pictures to be used as album covers are written in a **SYNOPHOTO_ALBUM.cover** file.

```
[parent path]/@eaDir/[album name]/SYNOPHOTO_ALBUM.cover
```

Thumbnails of media (pictures and videos) are stored in **SYNOPHOTO_THUMB_S.jpg** files (S: small, M: medium, B: big, L: large, XL: extra large).

```
[parent path]/@eaDir/[media name]/SYNOPHOTO_THUMB_S.jpg
```

More metadata files can be found but are not used by the webapp.

_Example, initial file structure_

```
photo
+-- User folder 1
|  +-- User subfolder A
|  | +-- Picture 1.jpg
|  | +-- Picture 2.jpg
+-- User folder 2
|  +-- Picture 3.jpg
```

_DS Photo managed file structure_

```
photo
+-- @eaDir
|  +-- User folder 1
|  |  +-- SYNOPHOTO_ALBUM.cover
|  +-- User folder 2
|  |  +-- SYNOPHOTO_ALBUM.cover
+-- User folder 1
|  +-- @eaDir
|  |  +-- User subfolder A
|  |  |  +-- SYNOPHOTO_ALBUM.cover
|  +-- User subfolder A
|  |  +-- @eaDir
|  |  |  +-- Picture 1.jpg
|  |  |  |  +-- SYNOPHOTO_THUMB_M.jpg
|  |  |  |  +-- SYNOPHOTO_THUMB_B.jpg
|  |  |  |  +-- SYNOPHOTO_THUMB_S.jpg
|  |  |  |  +-- SYNOPHOTO_THUMB_L.jpg
|  |  |  |  +-- SYNOPHOTO_THUMB_XL.jpg
|  |  |  +-- Picture 2.jpg
|  |  |  |  +-- SYNOPHOTO_THUMB_M.jpg
|  |  |  |  +-- SYNOPHOTO_THUMB_B.jpg
|  |  |  |  +-- SYNOPHOTO_THUMB_S.jpg
|  |  |  |  +-- SYNOPHOTO_THUMB_L.jpg
|  |  |  |  +-- SYNOPHOTO_THUMB_XL.jpg
|  |  +-- Picture 1.jpg
|  |  +-- Picture 2.jpg
+-- User folder 2
|  +-- @eaDir
|  |  +-- Picture 3.jpg
|  |  |  +-- SYNOPHOTO_THUMB_M.jpg
|  |  |  +-- SYNOPHOTO_THUMB_B.jpg
|  |  |  +-- SYNOPHOTO_THUMB_S.jpg
|  |  |  +-- SYNOPHOTO_THUMB_L.jpg
|  |  |  +-- SYNOPHOTO_THUMB_XL.jpg
|  +-- Picture 3.jpg
```