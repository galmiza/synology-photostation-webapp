<?php
$config = array(
  "tokens" => array(
    "AJ58GJ39GYHA" => [""],
    "I48N8730Y7V4" => ["vrac/Kiev"],
    "9J48VJ20UVHC" => ["2018/2018.12 - Noël en Alsace"],
    "0BGN73YIG8DP" => ["2018","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007"],
    "GI48FAMFH30N" => ["2007/2007.08 - ADS full/1 - Brésil/2 - Ilha Grande"]
  ),
  "extensions" => array("bmp","png","jpg","jpeg"),
  "root" => "../../photo/"
);
?>