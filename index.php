<?php

/*
 * Token protected API to:
 *  -render photos
 *  -list albums/photos as [{"type":"folder|file",path":"string","thumbnail":"string"}]
 */

$path = $_GET['path'];
$target = $_GET['target'];
$download = $_GET['download'];
$token = $_GET['token'];

// Load permissions
include("config.php");
$auth = $config["tokens"];
$root = $config["root"];

function isAuthorized($path) {
  global $auth, $token;
  $rej = true;
  foreach ($auth[$token] as $a) {
    if (substr($a,0,strlen($path))==$path) $rej = false;
    if (substr($path,0,strlen($a))==$a) $rej = false;
  }
  return !$rej;
}

/*
 * Render an image (or download as file if another format)
 */
if ($target) {
  if (isAuthorized($target)==false) exit(1);
  $ext = strtolower(substr(strrchr($target,"."),1));
  switch ($ext) {
    case "gif": $ctype="image/gif"; break;
    case "png": $ctype="image/png"; break;
    case "jpeg":
    case "jpg": $ctype="image/jpeg"; break;
    default:
      header('Content-Disposition: attachment; filename="'.basename($target).'"');
  }
  if ($download) header('Content-Disposition: attachment; filename="'.basename($target).'"');
  header('Content-type: '.$ctype);
  readfile($root.$target);


/*
 * Show list of images
 */
} else {
  $dict = array();

  if ($path) $search = $root.$path."/*";
  else $search = $root."*";
  foreach (glob($search) as $rel) {
    $filename = substr($rel,strlen($root));
    if (basename($filename)=="@eaDir") continue;
    if (isAuthorized($filename)==false) continue;

    // Process request
    $o = array();
    $o['path'] = $filename;
    if (is_dir($rel)) {
      $o['type'] = "folder";
      $covername = file_get_contents($root.$path."/@eaDir/".basename($filename)."/SYNOPHOTO_ALBUM.cover");
      if (basename($covername)==$covername) {
        $o['thumbnail'] = $filename."/@eaDir/".$covername."/SYNOPHOTO_THUMB_B.jpg";
      } else {
        $o['thumbnail'] = $filename."/".dirname($covername)."/@eaDir/".basename($covername)."/SYNOPHOTO_THUMB_B.jpg";
      }
    } else {
      $ext = pathinfo($filename, PATHINFO_EXTENSION);
      if (in_array(strtolower($ext), $config["extensions"])==false) continue;
      $o['type'] = "file";
      $o['thumbnail'] = dirname($filename)."/@eaDir/".basename($filename)."/SYNOPHOTO_THUMB_B.jpg";
      //$o['path'] = dirname($filename)."/@eaDir/".basename($filename)."/SYNOPHOTO_THUMB_XL.jpg"; // set target file as a big thumbnail
    }
    array_push($dict, $o);
  }
  echo json_encode($dict);
  header('Content-type: application/json');
}


?>